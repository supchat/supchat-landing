import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CustomerWidget from './CustomerWidget';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <CustomerWidget />
  </React.StrictMode>,
  document.getElementById('auth-widget')
);

ReactDOM.render(
  <React.StrictMode>
    <CustomerWidget />
  </React.StrictMode>,
  document.getElementById('auth-widget_top')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
