import React from 'react';
import './App.css';
import axios from "axios";
import Spinner from 'react-bootstrap/Spinner';
import 'bootstrap/dist/css/bootstrap.min.css';
import {apiKeyObserver} from './utils/ApiKeyObserver'

class CustomerWidget extends React.Component {


    componentDidMount() {
        apiKeyObserver.subscribe(data => {
            console.log('here2');
            this.setState({
                apiKey: data
            })
        });
        let apiKey = localStorage.getItem('apiKey');
        if (apiKey) {
            console.log('here');
            apiKeyObserver.broadcast(apiKey)
        }
    }

    constructor(props) {
        super(props);
        this.state = {value: '', progress: false};
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({value: event.target.value});
    };

    handleSubmit = async (event) => {
        const userEmail = {userEmail: this.state.value};
        this.setState(
            {progress: true}
        );
        let response = await axios.post('https://supchat.chat/back/create-customer', userEmail);
        let apiKey = response?.data?.uid;
        if (apiKey) {
            localStorage.setItem('apiKey', apiKey);
            apiKeyObserver.broadcast(apiKey)
        }
        this.setState(
            {progress: false, apiKey: apiKey}
        );
        console.log(`submit click ${JSON.stringify(response)}`);
    };

    render() {
        return (
            <div className="App">
                <div
                    className={this.state.apiKey || this.state.progress ? "auth_form_hidden" : "auth_form navbar_email_wrapper email_wrapper"}>
                    <form action="#" className="navbar_form">
                        <input type="text" className="email_input" title="Your email" onChange={this.handleChange}
                               placeholder="Your email"/>
                        <button type="submit" className="email_submit" onClick={this.handleSubmit}>Get it</button>
                    </form>
                </div>
                <div className={this.state.apiKey ? "auth_api_key" : "auth_api_key_hidden"}>
                        {"API KEY: " + this.state.apiKey}
                </div>
                <div className={this.state.progress ? "auth_progress" : "auth_progress_hidden"}>
                    <Spinner animation="border"/>
                </div>
            </div>
        );
    }

}

export default CustomerWidget;
