import React from 'react';
import { render } from '@testing-library/react';
import CustomerWidget from './CustomerWidget';

test('renders learn react link', () => {
  const { getByText } = render(<CustomerWidget />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
